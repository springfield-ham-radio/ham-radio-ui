# Ham Delux (ham-radio-ui)

Ham radio goodness

## Development Environment

If you develop with [VS Code](https://code.visualstudio.com), settings and extension recommendations are pre-configured.

This project is based on [NodeJS](https://nodejs.dev) 18.  It is recommended, but not required, to install NodeJS with [Volta](https://volta.sh).  The version of NodeJS is pinned in the package.json with Volta.

You will also need [yarn](https://yarnpkg.com).  If you are using Volta, you can install yarn with:

- volta install yarn


## Running From Source

- git clone https://gitlab.com/springfield-ham-radio/app/ham-radio-ui.git
- yarn install
- yarn dev

## Contrbuting

Report bugs and feature requests to: https://gitlab.com/springfield-ham-radio/app/ham-radio-ui/-/issues
For source contributions, please open a merge request: https://gitlab.com/springfield-ham-radio/app/ham-radio-ui/-/merge_requests
